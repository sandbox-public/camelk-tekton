# Running the Integration with CamelK

To run the CamelK integration directly in dev mode, execute:

```
kamel run \
--dev \
--name ob \
--property camel.rest.port=8080 \
--dependency=camel-rest \
--dependency camel-undertow \
--dependency camel-sql \
--dependency camel-jackson \
--dependency mvn:org.springframework.boot:spring-boot-starter-jdbc:1.5.16.RELEASE \
--dependency mvn:com.h2database:h2:1.4.193 \
--open-api ob-api.json \
--logging-level org.apache.camel.k=DEBUG \
--resource config/schema.sql \
ob.java
```

To test the integration, use the following `curl` command:

```
curl http://ob-bruno-camelk.apps-crc.testing/open-data/banks
```

# Running the pipeline

## Pre-requisites:

- Tekton Operator installed (cluster-wide)


## Instructions

1. Create a new project where to run Pipelines:
	
	```
	oc new-project tekton-pipelines
	```

	Create ServiceAccount to interact with CamelK

	```
	oc apply -f tekton/camel-k-pipeline-task-permissions.yaml
	```

	Create the Pipeline definition and resources

	```
	oc apply -f tekton/camel-k-pipeline-task-definition.yaml
	```

1. Prepare DEV environment:

	Create DEV namespace:

	```
	oc new-project camelk-dev-openbanking
	```

	Grant `edit` permissions to the pipeline *ServiceAccount*:

	```
	oc policy add-role-to-user edit system:serviceaccount:tekton-pipelines:pipeline
	```

	Install CamelK Operator.

	Create an IntegrationPlatform

1. Prepare PROD environment:

	Create PROD namespace:

	```
	oc new-project camelk-prod-openbanking
	```

	Grant `edit` permissions to the pipeline *ServiceAccount*:

	```
	oc policy add-role-to-user edit system:serviceaccount:tekton-pipelines:pipeline
	```

	Install CamelK Operator.

	Create an IntegrationPlatform

1. Run the pipeline:

	```
	oc apply -f tekton/camel-k-pipeline-task-run.yaml -n tekton-pipelines
	```